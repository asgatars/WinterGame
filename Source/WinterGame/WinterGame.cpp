// Copyright Epic Games, Inc. All Rights Reserved.

#include "WinterGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, WinterGame, "WinterGame" );

DEFINE_LOG_CATEGORY(LogWinterGame)
 