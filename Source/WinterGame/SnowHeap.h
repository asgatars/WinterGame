// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Interact.h"
#include "SnowHeap.generated.h"

UCLASS()
class WINTERGAME_API ASnowHeap : public AActor, public IInteract
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnowHeap();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Collision)
		class USphereComponent* SphereCollision;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Mwsh)
		class UStaticMeshComponent* Mesh;

	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact")
		void Interact(); //Prototype declaration
		virtual void Interact_Implementation() override; // actual declaration

};
