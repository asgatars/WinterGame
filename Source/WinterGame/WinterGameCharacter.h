// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Interact.h"
#include "GameFramework/Character.h"
#include "Components/SceneComponent.h"
#include "GenericTeamAgentInterface.h"
#include "WinterGameCharacter.generated.h"

UCLASS(Blueprintable)
class AWinterGameCharacter : public ACharacter, public IInteract, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

public:
	AWinterGameCharacter();

	// Called every frame.
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Ammo)
		class USceneComponent* ShootLoc;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Ammo)
		class UStaticMeshComponent* Bullet;
	
	//Set types of ammmo in ue Editor 
	UPROPERTY(EditDefaultsOnly, Category = "Ammo")
		TSubclassOf<AActor> SnowBall;
	
private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;



	//Overlap Event
	UFUNCTION()
		void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
								UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
								const FHitResult& SweepResult);
	UFUNCTION()
		void HandleEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
								UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	FGenericTeamId TeamId;
protected:
	//Ammo
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite, Category = "Ammo")
	int Ammo = 5;

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite, Category = "Helth")
	float Helth = 100;

	
	
	UFUNCTION()
		void SpawnSnowBall();
public:
	UFUNCTION()
		void CalcAmmo(int Value);
	UFUNCTION(BlueprintCallable)
		void Damage(float Value);

	
	UFUNCTION(BlueprintCallable)
		void Shoot();
	
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite, Category = "Ammo")
	bool bShoot = false;
	
	virtual FGenericTeamId GetGenericTeamId() const override { return TeamId; }
};

