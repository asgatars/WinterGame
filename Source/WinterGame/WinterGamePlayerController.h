// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "WinterGamePlayerController.generated.h"

UCLASS()
class AWinterGamePlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AWinterGamePlayerController();

protected:

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

public:
	//Create Movement Axis
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);

	float AxisX = 0.0f;
	float AxisY = 0.0f;


	//create Movement Tick  function
	UFUNCTION()
		void MovementTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
        void StartShoot();
	UFUNCTION(BlueprintCallable)
        void EndShoot();
	
};



