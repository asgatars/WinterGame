// Fill out your copyright notice in the Description page of Project Settings.


#include "HeapGenerator.h"

// Sets default values
AHeapGenerator::AHeapGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	BoxCollision->SetGenerateOverlapEvents(false);
	BoxCollision->SetRelativeScale3D(FVector(100.0f, 100.0f, 20.0f));
	
}

// Called when the game starts or when spawned
void AHeapGenerator::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHeapGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}