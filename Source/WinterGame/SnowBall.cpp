// Fill out your copyright notice in the Description page of Project Settings.


#include "SnowBall.h"





#include "MyEnemy.h"
#include "EnvironmentQuery/EnvQueryTypes.h"


// Sets default values
ASnowBall::ASnowBall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	Mesh->SetupAttachment(GetRootComponent());

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Collision"));
	SphereCollision->SetupAttachment(Mesh);
	SphereCollision->SetSphereRadius(20.0f);
	SphereCollision->SetGenerateOverlapEvents(true);

		
}

// Called when the game starts or when spawned
void ASnowBall::BeginPlay()
{
	Super::BeginPlay();
	// bind Overlap
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &ASnowBall::HandleBeginOverlap);
	SphereCollision->OnComponentEndOverlap.AddDynamic(this, &ASnowBall::HandleEndOverlap);
}

// Called every frame
void ASnowBall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Fly(DeltaTime);

}

void ASnowBall::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// execute interact if object of needed class
	if(Cast<AMyEnemy>(OtherActor))
	{
		IInteract::Execute_Interact(OtherActor);
		Destroy();
	}
}

void ASnowBall::HandleEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	
}

void ASnowBall::Fly(float DeltaTime)
{
		AddActorLocalOffset((Direction*Speed*DeltaTime), false);
}



