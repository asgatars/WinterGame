// Copyright Epic Games, Inc. All Rights Reserved.

#include "WinterGameCharacter.h"



#include "GenericTeamAgentInterface.h"
#include "SnowHeap.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/World.h"

AWinterGameCharacter::AWinterGameCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	Bullet = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SnowBallMEsh"));
	Bullet ->SetupAttachment(GetMesh());
	
	
	//Create a shoot location
	ShootLoc = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	ShootLoc->SetupAttachment(GetRootComponent());

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 600.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm


	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	TeamId = FGenericTeamId(0);
}

void AWinterGameCharacter::BeginPlay()
{
	Super::BeginPlay();

	// bind Overlap
	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &AWinterGameCharacter::HandleBeginOverlap);
	GetCapsuleComponent()->OnComponentEndOverlap.AddDynamic(this, &AWinterGameCharacter::HandleEndOverlap);
}

void AWinterGameCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
	
}

//event begin overlap
void AWinterGameCharacter::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(OtherActor->GetClass()->ImplementsInterface(UInteract::StaticClass()))
	{
		// execute interact if object of needed class
		if(Cast<ASnowHeap>(OtherActor))
		{
			IInteract::Execute_Interact(OtherActor);
			CalcAmmo(3);
		}
		
	}
	
}
//event end overlap
void AWinterGameCharacter::HandleEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	
}

void AWinterGameCharacter::SpawnSnowBall()
{
	//Set parameters for spawn actor
	FActorSpawnParameters SpawnParams;
	FVector SpawnLocation = ShootLoc->GetComponentLocation();
	FRotator SpawnRotation = ShootLoc->GetComponentRotation();
	//Spawn Actor
	GetWorld()->SpawnActor<AActor>(SnowBall, SpawnLocation, SpawnRotation, SpawnParams);
}

void AWinterGameCharacter::CalcAmmo(int Value)
{
	Ammo += Value; //  add ammo
}
// take Damage
void AWinterGameCharacter::Damage(float Value)
{
	if(Helth >0)
	Helth -= Value;
			
}

void AWinterGameCharacter::Shoot()
{
	if(Ammo > 0) //Check ammo in inventory
	{
		SpawnSnowBall();
		Ammo -= 1;
	}
	else
	{
		Ammo = 0;
	}

}




