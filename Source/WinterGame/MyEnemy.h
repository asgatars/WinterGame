// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/SphereComponent.h"
#include "MyGameStateBase.h"
#include "Particles/ParticleSystem.h"
#include "Components/AudioComponent.h"
#include "WinterGameCharacter.h"
#include "MyEnemy.generated.h"

UCLASS()
class WINTERGAME_API AMyEnemy : public ACharacter, public IInteract
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyEnemy();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Collision)
		class USphereComponent* SphereCollision;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Particle)
		class UParticleSystem* Particle;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Audio)
		class UAudioComponent* SnowballHit;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UAudioComponent* Death;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
    void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                            UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                            const FHitResult& SweepResult);
	
	UFUNCTION()
        void HandleEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                              UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact")
        void Interact(); //Prototype declaration
	virtual void Interact_Implementation() override; // actual declaration

	AGameStateBase* myGameState;
	
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite, Category = "Helth")
	bool bDead = false;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite, Category = "Helth")
	int Heath= 1;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite, Category = "Helth")
	float Damage= 10.f;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
	int Score = 10;
	
	
	bool bMakeDamage = false;
	float Timer = 0.0f;
	float Seconds = 0.0f;

	AWinterGameCharacter* Character;
	
	UFUNCTION(BlueprintCallable)
        void MakeDamage(float DeltaTime);
	UFUNCTION(BlueprintCallable)
        void SecondTick(float DeltaTime);
	
};




