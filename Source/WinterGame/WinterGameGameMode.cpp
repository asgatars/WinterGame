// Copyright Epic Games, Inc. All Rights Reserved.

#include "WinterGameGameMode.h"
#include "WinterGamePlayerController.h"
#include "WinterGameCharacter.h"
#include "UObject/ConstructorHelpers.h"

AWinterGameGameMode::AWinterGameGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AWinterGamePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}



