// Copyright Epic Games, Inc. All Rights Reserved.

#include "WinterGamePlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "WinterGameCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

AWinterGamePlayerController::AWinterGamePlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;

}

void AWinterGamePlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
	MovementTick(DeltaTime);
	
}

void AWinterGamePlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();
	//Movement
	InputComponent->BindAxis(TEXT("MoveForward"), this, &AWinterGamePlayerController::InputAxisX);
	InputComponent->BindAxis(TEXT("MoveRight"), this, &AWinterGamePlayerController::InputAxisY);
	//Shoot
	InputComponent->BindAction(TEXT("Shoot"), IE_Pressed, this, &AWinterGamePlayerController::StartShoot);
	InputComponent->BindAction(TEXT("Shoot"), IE_Released, this, &AWinterGamePlayerController::EndShoot);
	
}

void AWinterGamePlayerController::InputAxisX(float Value)
{
	AxisX = Value;
}

void AWinterGamePlayerController::InputAxisY(float Value)
{
	AxisY = Value;
}

void AWinterGamePlayerController::MovementTick(float DeltaTime)
{
	if(AWinterGameCharacter* MyPawn = Cast<AWinterGameCharacter>(GetPawn()))
	{
		MyPawn->AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		MyPawn->AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

		//Rptate To Cursor
		APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if(MyController)
		{
			FVector WorldLocation;
			FVector WorldDirection;
			MyController->DeprojectMousePositionToWorld(WorldLocation, WorldDirection);
			FVector CurDirection = (WorldDirection* 500000.0f) + WorldDirection;
			FVector PlaneNormal (0.0f, 0.0f, 1.0f);
			FVector Intersection;
			float T = -1.0f;
			UKismetMathLibrary::LinePlaneIntersection_OriginNormal(WorldLocation, CurDirection,
			                                                       MyPawn->GetActorLocation(), PlaneNormal, T,
			                                                       Intersection);
			float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(MyPawn->GetActorLocation(), Intersection).Yaw;
			MyPawn->SetActorRotation(FQuat(FRotator(0.0f, FindRotaterResultYaw, 0.0f)));
		}
	}
}

inline void AWinterGamePlayerController::StartShoot()
{
	if(AWinterGameCharacter* MyPawn = Cast<AWinterGameCharacter>(GetPawn()))
	{
		MyPawn->Shoot();
		MyPawn->bShoot=true;
		MyPawn->Bullet->SetVisibility(false);
	}
}

inline void AWinterGamePlayerController::EndShoot()
{
	if(AWinterGameCharacter* MyPawn = Cast<AWinterGameCharacter>(GetPawn()))
	{
		MyPawn->bShoot=false;
		MyPawn->Bullet->SetVisibility(true);
	}
}



