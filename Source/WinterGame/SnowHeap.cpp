// Fill out your copyright notice in the Description page of Project Settings.

#include "SnowHeap.h"
#include "WinterGameCharacter.h"




// Sets default values
ASnowHeap::ASnowHeap()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(GetRootComponent());
	
	//Create collision for interact with Character
	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	SphereCollision->SetupAttachment(Mesh);
	SphereCollision->SetGenerateOverlapEvents(true);

 }

// Called when the game starts or when spawned
void ASnowHeap::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASnowHeap::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnowHeap::Interact_Implementation()
{
	Destroy();
}



