// Fill out your copyright notice in the Description page of Project Settings.


#include "MyEnemy.h"

#include "NavigationSystemTypes.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
AMyEnemy::AMyEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Collision"));
	SphereCollision->SetupAttachment(GetCapsuleComponent());
	SphereCollision->SetSphereRadius(120.0f);

	Particle = CreateDefaultSubobject<UParticleSystem>(TEXT("Particle"));

	//create audio component
	SnowballHit = CreateDefaultSubobject<UAudioComponent>(TEXT("Audio"));
	SnowballHit->SetPaused(true);

	Death = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioDeath"));
	Death->SetPaused(true);
	
}

// Called when the game starts or when spawned
void AMyEnemy::BeginPlay()
{
	Super::BeginPlay();

	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &AMyEnemy::HandleBeginOverlap);
	SphereCollision->OnComponentEndOverlap.AddDynamic(this, &AMyEnemy::HandleEndOverlap);

	myGameState = GetWorld()->GetGameState();
}

// Called every frame
void AMyEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SecondTick(DeltaTime);
	MakeDamage(DeltaTime);
}

// Called to bind functionality to input
void AMyEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AMyEnemy::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
    UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(Cast<AWinterGameCharacter>(OtherActor))
	{
		Character = Cast<AWinterGameCharacter>(OtherActor);
		bMakeDamage = true;
				
	}
}

void AMyEnemy::HandleEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
    UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex)
{
	bMakeDamage = false;
}

void AMyEnemy::Interact_Implementation()
{
	AMyGameStateBase* GameState = Cast<AMyGameStateBase>(myGameState);	

	if(GameState)
	{
		Heath -= 1;
		SnowballHit->SetPaused(false);
		if(Heath <= 0)
		{
			SphereCollision->SetGenerateOverlapEvents(false);
			GetCapsuleComponent()->SetGenerateOverlapEvents(false);
			SnowballHit->Play();
			GameState->SumScore(Score);
			Death->SetPaused(false);
			bDead = true;
			GetMovementComponent()->DestroyComponent();
			Seconds = 2.f;
		}
		
	}
	
}

void AMyEnemy::SecondTick(float DeltaTime)
{
	if(Seconds > 0)
	{
		Seconds -= DeltaTime;
	}
	else if (bDead == true)
	{
		Seconds = 0.0f;
		Destroy();
	}
	else
	{
		Seconds = 0.0f;
		
	}
}

void AMyEnemy::MakeDamage(float DeltaTime)
{
	if(bMakeDamage)
	{
		if(Timer > 0)
		{
			Timer -= DeltaTime;
		}
		else
		{
			Timer = 0;
			Character->Damage(Damage);
			Timer = 4;
		}
		
	}
	
}